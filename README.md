# NoLiquidTP
This plugin disallows a player from using certain commands when in water or lava.

## Commands
This plugin does not require any commands.

## Permissions
If a player has the permission 
<pre>
nltp.ignore
</pre>
, then the plugin will ignore them and they will be able to use any command that they normally can when in water/lava.

## Config
In the config, you can set the commands to be blocked when a player is in water or lava.
<br>Here is the default config as an example:
<pre>
blocked-commands:
 - 'tpa'
 - 'tpaccept'
 - 'sethome'
 - 'tp'
no-perms-msg: '&3[NoLiquidTP] &cYou don''t have permission to use this command while in water/lava!'
</pre>
This config will block the following commands: 
<pre>
/tpa
/tpaccept
/sethome
/tp
</pre>
and the message to display to the user if they are stopped from using a command in water/lava is stored in 'no-perms-msg'.
Use this as an example for the format.
