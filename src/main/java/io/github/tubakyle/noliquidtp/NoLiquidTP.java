package io.github.tubakyle.noliquidtp;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle on 7/17/2014.
 */
public class NoLiquidTP extends JavaPlugin implements Listener {
        public void onEnable() {
            saveDefaultConfig();
            getServer().getPluginManager().registerEvents(this, this);
            getConfig().options().copyDefaults(false);
            if (!getConfig().isSet("no-perms-msg")) {
                //upgrade config from v1.1 to v1.2
                getConfig().set("no-perms-msg", "&3[NoLiquidTP] &cYou don't have permission to use this command while in water/lava!");
                saveConfig();
            }
        }

        @EventHandler
        public void preCommand(PlayerCommandPreprocessEvent e) {
            reloadConfig();
            Player p = e.getPlayer();
            if (p.getLocation().getBlock().isLiquid()) {
                if(!p.hasPermission("nltp.ignore")) {
                    for (String s : getConfig().getStringList("blocked-commands")) {
                        if (e.getMessage().startsWith("/" + s)) {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("no-perms-msg")));
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
